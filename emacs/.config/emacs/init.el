(require 'package)
(setq package-enable-at-startup nil)

;; Set up some package respositories.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; Test use-package to be installed, otherwise installs it.
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(let ((path (shell-command-to-string ". ~/.zshrc; echo -n $PATH")))
  (setenv "PATH" path)
  (setq exec-path 
        (append
         (split-string-and-unquote path ":")
         exec-path)))

(org-babel-load-file
 (expand-file-name
  (concat user-emacs-directory "/main.org")))

(eval-when-compile
  (require 'use-package))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(avy-timeout-seconds 0.3 nil nil "Customized with use-package avy")
 '(beacon-color "#ff9da4")
 '(column-number-mode t)
 '(custom-safe-themes
   '("ffef467dfed832df46d4e188049e52aad1d64c16070484fc6b62f158ece95471" "306d853c5b47e1baf5e815eb49daa8a46d7f54d3f5ab624f3b30a6c1eb8e1f0c" "544bb10f6c6d7338be3bc73d147f26273d62e094e7643bfa842dffa9d742e30a" "2a7d6f6a46b1b8504b269029a7375597f208a5cdfa1ea125b09255a592bb326e" "33876ef6caf9c4cb35f8bc9430fb64ba503036a4e80532bb764edec121900248" "e4013fb0675182b04aa22f29054842db418d5a72da7ebb3c5b9dbb624502aaec" "f48be80177f0d9a2b19d8dc19f3903d9be3c4d885d110e82b591d1184586fad0" "b64a60e69617b4348d0402fad2f0d08a694301132e7ab243dab4d6a65c3bf948" "46c65f6d9031e2f55b919b1486952cddcc8e3ee081ade7eb2ffb6a68a804d30e" "cd65fad3243fb2b04660fb5c56152e27030e904b5f06b743bb77cac85c5327b7" "74367676a7b0562975704f8e576d5e103451527b36c9226a013cd8f3ae2140f5" "6a94122cfa72865c9b7a211ee461e4cc8834451d035fb43ffa478a630dec3d5b" "dbc6d947d551aa03090daf6256233454c6a63240e17a8f3d77889d76fef1749d" "e16cd3e6093cf35e65dbac6c7649931936f32f56be34477cb7cbe1ee332c5b99" "0af4fc8329f73bd771df30015858813385461513e044df730fc805a49f5ece52" "cc626529cf94f8fd259ebd67e7e85af48b080fd8055f70cd7c4f1c0c86c91179" "84c2c93ce268699838b51eeeaaec46e064d5048e232b39113b73209be3ef6fd4" "72cc2c6c5642b117034b99dcc3a33ff97a66593429c7f44cd21b995b17eebd4e" "c2efe6f5e2bd0bddfb2d6e26040545768939d2029f77e6b6a18d1ee0e0cb1033" "234c3805fb341b7ce2a9e8ce6d72dba9b81e9335422cfee838e128edfb8a9774" "b66970f42d765a40fdb2b6b86dd2ab6289bed518cf4d8973919e5f24f0ca537b" "249e100de137f516d56bcf2e98c1e3f9e1e8a6dce50726c974fa6838fbfcec6b" "06ed754b259cb54c30c658502f843937ff19f8b53597ac28577ec33bb084fa52" "e266d44fa3b75406394b979a3addc9b7f202348099cfde69e74ee6432f781336" "e8567ee21a39c68dbf20e40d29a0f6c1c05681935a41e206f142ab83126153ca" "a131602c676b904a5509fff82649a639061bf948a5205327e0f5d1559e04f5ed" "c95813797eb70f520f9245b349ff087600e2bd211a681c7a5602d039c91a6428" "2ce76d65a813fae8cfee5c207f46f2a256bac69dacbb096051a7a8651aa252b0" "11cc65061e0a5410d6489af42f1d0f0478dbd181a9660f81a692ddc5f948bf34" "9cd57dd6d61cdf4f6aef3102c4cc2cfc04f5884d4f40b2c90a866c9b6267f2b3" "f00a605fb19cb258ad7e0d99c007f226f24d767d01bf31f3828ce6688cbdeb22" "d516f1e3e5504c26b1123caa311476dc66d26d379539d12f9f4ed51f10629df3" "d9a28a009cda74d1d53b1fbd050f31af7a1a105aa2d53738e9aa2515908cac4c" "3c7a784b90f7abebb213869a21e84da462c26a1fda7e5bd0ffebf6ba12dbd041" "6128465c3d56c2630732d98a3d1c2438c76a2f296f3c795ebda534d62bb8a0e3" "2f945b8cbfdd750aeb82c8afb3753ebf76a1c30c2b368d9d1f13ca3cc674c7bc" "34dc2267328600f3065630e161a8ae59939700684c232073cdd5afbf78456670" "0f1733ad53138ddd381267b4033bcb07f5e75cd7f22089c7e650f1bb28fc67f4" "fa477d10f10aa808a2d8165a4f7e6cee1ab7f902b6853fbee911a9e27cf346bc" "7d4340a89c1f576d1b5dec57635ab93cdc006524bda486b66d01a6f70cffb08e" "a9d67f7c030b3fa6e58e4580438759942185951e9438dd45f2c668c8d7ab2caf" "e62b66040cb90a4171aa7368aced4ab9d8663956a62a5590252b0bc19adde6bd" "ef07cb337554ffebfccff8052827c4a9d55dc2d0bc7f08804470451385d41c5c" "ff829b1ac22bbb7cee5274391bc5c9b3ddb478e0ca0b94d97e23e8ae1a3f0c3e" "11e0bc5e71825b88527e973b80a84483a2cfa1568592230a32aedac2a32426c1" "51043b04c31d7a62ae10466da95a37725638310a38c471cc2e9772891146ee52" "030346c2470ddfdaca479610c56a9c2aa3e93d5de3a9696f335fd46417d8d3e4" "886fe9a7e4f5194f1c9b1438955a9776ff849f9e2f2bbb4fa7ed8879cdca0631" "a22f40b63f9bc0a69ebc8ba4fbc6b452a4e3f84b80590ba0a92b4ff599e53ad0" "43c808b039893c885bdeec885b4f7572141bd9392da7f0bd8d8346e02b2ec8da" "427fa665823299f8258d8e27c80a1481edbb8f5463a6fb2665261e9076626710" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "75d3dde259ce79660bac8e9e237b55674b910b470f313cdf4b019230d01a982a" "ed85d92735406d8b9dc96b1b7f1e1c19a908bcb550e6dcaccdbe62e43d6efbeb" "151bde695af0b0e69c3846500f58d9a0ca8cb2d447da68d7fbf4154dcf818ebc" "1cfc3c062790a8d6f9ce677c50cf671609f45c32695778873b4a7619f1e749b5" "cd736a63aa586be066d5a1f0e51179239fe70e16a9f18991f6f5d99732cabb32" "ab9456aaeab81ba46a815c00930345ada223e1e7c7ab839659b382b52437b9ea" "9954ed41d89d2dcf601c8e7499b6bb2778180bfcaeb7cdfc648078b8e05348c6" "e3c87e869f94af65d358aa279945a3daf46f8185f1a5756ca1c90759024593dd" "e838d6375a73fda607820c65eb3ea1f9336be7bd9a5528c9161e10c4aa663b5b" "256bd513a9875cd855077162cdfee8d75b0ad7e18fe8b8cbc10412561fbef892" "4e10cdf7d030fb41061cf57c74f6ddfc19db8d4af6c8e0723dc77f9922543a3d" "a3fa4abaf08cc169b61dea8f6df1bbe4123ec1d2afeb01c17e11fdc31fc66379" "fd944f09d4d0c4d4a3c82bd7b3360f17e3ada8adf29f28199d09308ba01cc092" "af4dc574b2f96f5345d55b98af024e2db9b9bbf1872b3132bc66dffbf5e1ba1d" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "34c99997eaa73d64b1aaa95caca9f0d64229871c200c5254526d0062f8074693" "6b289bab28a7e511f9c54496be647dc60f5bd8f9917c9495978762b99d8c96a0" "955426466aa729d7d32483d3b2408cf474a1332550ad364848d1dfe9eecc8a16" "95d17369f041df4d642bee73b7d57cffeba240877d73d22688f1a84ed317e96b" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "ffa6fbce4ffcec1f970f3430c927604c34bc1de2eb8e1454837a7f9d9f9b1bc5" default))
 '(evil-leader/no-prefix-mode-rx '("magit-.*-mode" "gnus-.*-mode"))
 '(fci-rule-color "#003f8e")
 '(flycheck-color-mode-line-face-to-color 'mode-line-buffer-id)
 '(frame-background-mode 'dark)
 '(git-gutter-fr:side #'right-fringe nil nil "positione the fringe in the right side")
 '(jdee-db-active-breakpoint-face-colors (cons "#1c1f24" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1c1f24" "#7bc275"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1c1f24" "#484854"))
 '(linum-relative-backend 'display-line-numbers-mode nil nil "Customized with use-package linum-relative")
 '(linum-relative-current-symbol "" nil nil "Customized with use-package linum-relative")
 '(objed-cursor-color "#ff665c")
 '(package-selected-packages
   '(memoize nix-mode tree-sitter-langs dap-node tree-sitter typescript-mode dap-mode clang-format lsp-haskell lsp-ui ef-themes lsp-mode rustic flycheck-irony company-irony irony counsel-gtags flycheck-haskell flycheck-clojure intero htmlize org-pomodoro kaolin-themes doom-themes doom-modeline yasnippet-snippets which-key web-mode use-package tomatinho spaceline-all-the-icons slime rainbow-delimiters racer paredit ox-twbs organic-green-theme org-bullets notmuch neotree magit linum-relative ivy-posframe hydra gruvbox-theme git-gutter-fringe flycheck-rust evil-leader emmet-mode editorconfig django-theme diminish counsel-projectile company-c-headers color-theme-sanityinc-tomorrow cider cargo beacon))
 '(pdf-view-midnight-colors '("#fdf4c1" . "#1d2021"))
 '(right-fringe-width 10 t nil "set the fringe size")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#ff9da4")
     (40 . "#ffc58f")
     (60 . "#ffeead")
     (80 . "#d1f1a9")
     (100 . "#99ffff")
     (120 . "#bbdaff")
     (140 . "#ebbbff")
     (160 . "#ff9da4")
     (180 . "#ffc58f")
     (200 . "#ffeead")
     (220 . "#d1f1a9")
     (240 . "#99ffff")
     (260 . "#bbdaff")
     (280 . "#ebbbff")
     (300 . "#ff9da4")
     (320 . "#ffc58f")
     (340 . "#ffeead")
     (360 . "#d1f1a9")))
 '(vc-annotate-very-old-color nil)
 '(window-divider-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
